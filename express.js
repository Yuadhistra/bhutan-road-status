const app = require('./app')
/* Starting the server on the port 4001. */
const port = 4001
app.listen(port,()=>{
    console.log(`App running on port ${port} ..`)
})